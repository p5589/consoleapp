﻿using System;
using System.IO;
using System.Linq;


namespace BookburnersDag31
{
    class Program
    {
        static void Main(string[] args)
        {
            //Main Menu, na check gebruiker(geeft gebruier mee voor verder gebruik). Keuze maken en verder, ultieme try/catch.  
            double kassaBedrag = 0;
            bool afsluiten = false, aankoop = true, verkoop = true, aanpassen = true, taxatie = true, kassa = true, backup = true, accounts = true;
            int hoofdmenukeuze;
            string gebruiker = "", title = @" *   )      )              (                   )       (                                          
` )  /(   ( /(     (      ( )\               ( /(     ( )\     (    (               (    (         
 ( )(_))  )\())   ))\     )((_)   (     (    )\())    )((_)   ))\   )(     (       ))\   )(    (   
(_(_())  ((_)\   /((_)   ((_)_    )\    )\  ((_)\    ((_)_   /((_) (()\    )\ )   /((_) (()\   )\  
|_   _|  | |(_) (_))      | _ )  ((_)  ((_) | |(_)    | _ ) (_))(   ((_)  _(_/(  (_))    ((_) ((_) 
  | |    | ' \  / -_)     | _ \ / _ \ / _ \ | / /     | _ \ | || | | '_| | ' \)) / -_)  | '_| (_-< 
  |_|    |_||_| \___|     |___/ \___/ \___/ |_\_\     |___/  \_,_| |_|   |_||_|  \___|  |_|   /__/ ";
            try
            {
                Account(ref gebruiker);

                if (System.IO.File.Exists("KassaInhoud.txt"))
                {
                    using (StreamReader reader = new StreamReader("KassaInhoud.txt"))
                    {
                        kassaBedrag = Convert.ToDouble(reader.ReadLine());
                    }
                }
                else
                {
                    using (StreamWriter writer = new StreamWriter("KassaInhoud.txt"))
                    {
                        writer.WriteLine(kassaBedrag);
                    }
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine("KassaInhoud.txt werd niet gevonden. File is aangemaakt.");
                    Console.ResetColor();
                    Console.ReadKey();
                }

                while (afsluiten == false)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(title);
                    Console.ResetColor();
                    Console.WriteLine($"Antiquariaat/Boekenhandel TheBookBurners\n\nHOOFDMENU\n");
                    Console.WriteLine($"1.)Aankoop Boeken/Opslaan Nieuw Boek in Collectie\n\n2.)Verkopen van een boek\n\n" +
                        $"3.)Collectie Raadplegen/Aanpassen/Boeken Verbranden\n\n4.)Een boek taxeren/Taxaties bekijken/Taxaties wijzigen\n\n5.)De kassa beheren\n\n6.)BackUps maken van je gegevens\n\n7.)Gebruikersmenu\n\n8.)Afsluiten\n\n");
                    Console.WriteLine($"Geef uw keuze in 1/2/3/4/5/6/7/8");
                    int.TryParse(Console.ReadLine(), out hoofdmenukeuze);
                    Console.Clear();
                    switch (hoofdmenukeuze)
                    {
                        case 1:
                            while (aankoop == true)
                            {
                                aankoop = Aankopen(ref gebruiker);
                                Console.Clear();
                            }
                            break;
                        case 2:
                            while (verkoop == true)
                            {
                                verkoop = Verkopen(ref gebruiker);
                                Console.Clear();
                            }
                            break;
                        case 3:
                            while (aanpassen == true)
                            {
                                aanpassen = CollectieMain();
                                Console.Clear();
                            }
                            break;
                        case 4:
                            while (taxatie == true)
                            {
                                taxatie = TaxatieMain();
                                Console.Clear();
                            }

                            break;
                        case 5:
                            while (kassa == true)
                            {
                                kassa = KassaMain(ref kassaBedrag);
                                Console.Clear();
                            }
                            break;
                        case 6:
                            while (backup == true)
                            {
                                backup = BackUp();
                                Console.Clear();
                            }
                            break;
                        case 7:
                            while (accounts == true)
                            {
                                accounts = AccountMain();
                                Console.Clear();
                            }
                            break;
                        case 8:
                            afsluiten = true;
                            using (StreamWriter writer = new StreamWriter("KassaInhoud.txt"))
                            {
                                writer.WriteLine(kassaBedrag);
                            }
                            Console.WriteLine("Het programma wordt afgesloten, druk op een toets.");
                            Console.ReadLine();
                            break;
                        default:
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.WriteLine("Foute input, probeer opnieuw.");
                            Console.ResetColor();
                            break;
                    }
                    aankoop = true;
                    verkoop = true;
                    aanpassen = true;
                    taxatie = true;
                    backup = true;
                    accounts = true;
                }
            }
            catch
            {
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine("Fatale fout in de applicatie, Algemene WinkelVerbrandingsmodus Gestart! Evacueer! Slecht gewerkt Bart en Harald!");
                Console.ResetColor();
                Console.ReadLine();
            }
        }
        public static bool Aankopen(ref string a)
        {
            //Aankoop voor Winkel van aangebrachte boeken door klant. Moeten EERST getaxeerd worden voor ze kunnen aangekocht worden!
            //Sequentieel proces, automatische prijsaanpassing naar  verkoopsprijs, automatische kassa berekening na aankoop, drukken etiketten.
            // Collectie wordt geupdate bij aankoop.
            int isbn;
            bool aankopen, aanwezig, getaxeerd, doorgaan = true, etiket, aankoopbewijs, antwoord;
            string ticket, bewijs, boek, inhoud, gebruiker;
            double prijs;
            gebruiker = a;
            while (doorgaan == true)
            {
                Console.WriteLine("Geef het ISBN nummer van het boek in. Dit getal bestaat uit 9 cijfers");
                int.TryParse(Console.ReadLine(), out isbn);
                aanwezig = CollectieCheckIsbn(isbn);
                getaxeerd = TaxatieCheckISBN(isbn);
                if ((getaxeerd == false) && (aanwezig == false))
                {
                    Console.WriteLine("Dit boek is nog niet aanwezig in de collectie of getaxeerd, gelieve het eerst te taxeren.");
                    Console.ReadLine();
                }
                else if ((getaxeerd == true) && (aanwezig == false))
                {
                    Console.WriteLine("Dit boek is nog niet aanwezig in de collectie maar is reeds getaxeerd:");
                    boek = TaxatieDisplayResult(isbn);
                    Console.WriteLine(boek);
                    Console.WriteLine("Wilt u dit boek opnemen?");
                    bool.TryParse(Console.ReadLine(), out antwoord);
                    if (antwoord == true)
                    {
                        string[] arrBoek = boek.Split('|');
                        prijs = Convert.ToDouble(arrBoek[3]);
                        prijs = PrijsBerekenen(prijs);
                        arrBoek[3] = prijs.ToString("0.00");
                        boek = "";
                        for (int i = 0; i < arrBoek.Length; i++)
                        {
                            boek += (arrBoek[i] + "|");
                        }
                        boek = boek.Remove(boek.LastIndexOf('|'));
                        boek += "*";
                        using (StreamReader lezer = new StreamReader("Collectie.txt"))
                            inhoud = lezer.ReadToEnd();
                        inhoud += boek;

                        using (StreamWriter schrijver = new StreamWriter("Collectie.txt"))
                            schrijver.Write(inhoud);
                        Console.WriteLine("Boek opgenomen in collectie, kassa aangepast");
                        Console.WriteLine("Wil je een etiket afdrukken voor de winkel?");
                        bool.TryParse(Console.ReadLine(), out etiket);
                        if (etiket == true)
                        {
                            ticket = DrukEtiket(boek);
                            Console.WriteLine(ticket);
                            Console.ReadLine();
                        }
                        Console.WriteLine("Wil je een aankoopbewijs afdrukken voor de winkel?");
                        bool.TryParse(Console.ReadLine(), out aankoopbewijs);
                        if (aankoopbewijs == true)
                        {
                            bewijs = EtiketAankoop(boek, ref gebruiker);
                            Console.WriteLine(bewijs);
                            Console.ReadLine();
                        }
                        aanwezig = true;
                    }
                }
                else  //eLSE IF HIER
                {
                    Console.WriteLine("Dit boek is aanwezig! Wilt u een ander boek toevoegen?");
                    bool.TryParse(Console.ReadLine(), out doorgaan);
                }
            }
            aankopen = false;
            return aankopen;
        }
        public static bool Verkopen(ref string a)
        {
            //Verkopen van boeken aan Klant die aanwezig zijn in de winkel, kassafunctie, collectieupdate, etiketten drukken, sequentieel proces
            bool doorgaan = true, verkoop = false, etiket, verkopen;
            int isbn;
            string boek = "", ticket, gebruiker, inhoud;
            double prijs;

            gebruiker = a;
            while (doorgaan == true)
            {
                Console.WriteLine("Geef het ISBN nummer van het boek in. Dit getal bestaat uit 9 cijfers");
                int.TryParse(Console.ReadLine(), out isbn);
                if ((isbn.ToString()).Length == 9)
                {
                    boek = CollectieDisplayResult(isbn);
                    Console.WriteLine(boek);
                    if (boek != "")
                    {
                        Console.WriteLine($"\nWilt u dit boek verkopen?");
                        bool.TryParse(Console.ReadLine(), out verkoop);
                        if (verkoop == true)
                        {
                            string[] arrBoek = boek.Split('|');
                            prijs = Convert.ToDouble(arrBoek[3]);
                            Console.WriteLine($"\nVerkoopsprijs is {prijs.ToString("0.00")} euro.");
                            Console.WriteLine("Wil je een verkoopbewijs afdrukken voor de klant?");
                            bool.TryParse(Console.ReadLine(), out etiket);
                            if (etiket == true)
                            {
                                ticket = EtiketVerkoop(boek, ref gebruiker);
                                Console.WriteLine(ticket);
                                Console.ReadLine();
                            }
                            using StreamReader lezer = new StreamReader("Collectie.txt");
                            inhoud = lezer.ReadToEnd();
                            lezer.Close();
                            Console.WriteLine($"{boek}\nDit boek wordt verwijderd uit de collectie");
                            boek += "*";
                            inhoud = inhoud.Remove((inhoud.IndexOf(boek)), boek.Length);
                            using StreamWriter schrijver = new StreamWriter("Collectie.txt");
                            schrijver.WriteLine(inhoud);
                            schrijver.Close();
                            Console.ReadLine();
                        }
                    }
                    Console.WriteLine($"\nWilt u een ander boek verkopen?");
                    bool.TryParse(Console.ReadLine(), out doorgaan);
                }
                else
                {
                    Console.WriteLine("Ongeldige input, geef isbn van 9 cijfers in");
                }
            }
            verkopen = false;
            return verkopen;
        }   //AANGEPAST
        public static bool CollectieMain()
        {
            //Main menu om collectie te beheren, keuze maken
            int keuze;
            bool doorgaan = true, edit = false, remove = false, antwoord;
            string inhoud = "";

            while (doorgaan == true)
            {
                Console.WriteLine($"\nCollectieMenu\n\n1.) Collectie Weergeven\n\n2.) Een entry aanpassen\n\n3.) Boeken Verbranden\n\n4.)Terug Naar Hoofdmenu");
                int.TryParse(Console.ReadLine(), out keuze);

                switch (keuze)
                {
                    case 1:
                        using (StreamReader lezer = new StreamReader("Collectie.txt"))
                            inhoud = lezer.ReadToEnd();
                        string[] arrCollectie = inhoud.Split('*');
                        for (int i = 0; i < arrCollectie.Length; i++)
                        {
                            Console.WriteLine(arrCollectie[i]);
                        }
                        break;
                    case 2:
                        while (edit != true)
                        {
                            edit = CollectieEdit();
                            Console.Clear();
                        }
                        break;
                    case 3:
                        while (remove != true)
                        {
                            remove = CollectieRemove();
                            Console.Clear();
                        }
                        break;
                    case 4:
                        doorgaan = false;
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Foute keuze!");
                        break;
                }
                remove = false;
                edit = false;
            }
            antwoord = false;
            return antwoord;
        }
        public static bool TaxatieMain()
        {
            //idem Collectiemain
            bool afsluitenMenuTax = false, afsluitenTaxatie = false, antwoord = false;
            string antwoordTaxatie = "", inhoudTax = "";
            int taxatiemenukeuze = 0;
            int isbn = 0;

            Random myRandom = new Random();
            try
            {
                while (afsluitenMenuTax == false)
                {
                    Console.Clear();
                    Console.WriteLine($"Taxatie\n-------");
                    Console.WriteLine($"1.)Overzicht getaxeerde boeken\n\n2.)Nieuwe Taxatie maken\n\n" +
                        $"3.)Bestaande Taxatie Aanpassen\n\n4.)Terug naar Hoofdmenu\n\n");
                    Console.WriteLine($"Geef uw keuze in 1/2/3/4");
                    while (!(int.TryParse(Console.ReadLine(), out taxatiemenukeuze)))
                    {
                        Console.WriteLine("Gelieve een getal tussen 1 en 4 in te geven om uw keuze te maken.");   //Extra check indien iets anders dan int ingegeven wordt!
                    }
                    afsluitenTaxatie = false;

                    switch (taxatiemenukeuze)
                    {
                        case 1:
                            Console.Clear();
                            {
                                using (StreamReader lezer = new StreamReader("Taxatie.txt"))
                                {
                                    inhoudTax = lezer.ReadToEnd();
                                }
                                string[] arrTaxaties = inhoudTax.Split('*');
                                for (int i = 0; i < arrTaxaties.Length; i++)
                                {
                                    Console.WriteLine(arrTaxaties[i]);
                                }

                            }
                            Console.WriteLine("Druk op ENTER om terug te gaan naar het Hoofdmenu.");    //Hier nog enkele opties toevoegen?
                            Console.ReadLine();

                            break;
                        case 2:
                            while (afsluitenTaxatie == false)
                            {
                                Console.WriteLine("Geef het ISBN-nummer in van het boek dat u wilt taxeren:");

                                while (!(int.TryParse(Console.ReadLine(), out isbn) && isbn.ToString().Length == 9))
                                {
                                    Console.WriteLine("Dit is geen geldig ISBN-nummer, dit moet bestaan uit 9 cijfers!");
                                }

                                if (TaxatieCheckISBN(isbn) == true)
                                {
                                    Console.WriteLine("Wij hebben dit boek reeds getaxeerd:");
                                    Console.WriteLine(TaxatieDisplayResult(isbn));
                                }
                                else
                                {
                                    Console.WriteLine(TaxatieNieuw(isbn));
                                    Console.WriteLine("Taxatie uitgevoerd en toegevoegd aan de lijst met Taxaties.");
                                }

                                do
                                {
                                    Console.WriteLine("Wilt u nog een boek taxeren? ja/nee");
                                    antwoordTaxatie = Console.ReadLine();
                                    if (antwoordTaxatie == "nee")   //gaat uit deze loop EN uit case 2 while loop, terug naar Taxatie menu
                                    {
                                        afsluitenTaxatie = true;
                                    }
                                    else if (antwoordTaxatie != "ja" && antwoordTaxatie != "nee")
                                    {
                                        Console.WriteLine("Gelieve enkel met 'ja' of 'nee' te antwoorden!");
                                    }
                                } while (antwoordTaxatie != "ja" && antwoordTaxatie != "nee");           //bij antwoord 'ja' -> gaat uit deze loop, maar blijft in case 2 while loop
                            }
                            break;
                        case 3:

                            while (afsluitenTaxatie == false)   //Nog testen of deze flow werkt
                            {
                                TaxatieEdit();
                                do
                                {
                                    Console.WriteLine("Wilt u nog een taxatie aanpassen? ja/nee");
                                    antwoordTaxatie = Console.ReadLine();
                                    if (antwoordTaxatie == "nee")   //gaat uit deze loop EN uit case 2 while loop, terug naar Taxatie menu
                                    {
                                        afsluitenTaxatie = true;
                                    }
                                    else if (antwoordTaxatie != "ja" && antwoordTaxatie != "nee")
                                    {
                                        Console.WriteLine("Gelieve enkel met 'ja' of 'nee' te antwoorden!");
                                    }
                                } while (antwoordTaxatie != "ja" && antwoordTaxatie != "nee");           //bij antwoord 'ja' -> gaat uit deze loop, maar blijft in case 2 while loop
                            }
                            break;
                        case 4:
                            afsluitenMenuTax = true;
                            break;
                        default:
                            Console.WriteLine("Gelieve een getal tussen 1 en 4 in te geven om uw keuze te maken.");
                            break;
                    }
                }
            }
            catch
            {
                Console.WriteLine("Fatale fout in de applicatie, contacteer de IT afdeling van de The Book Burners!");

            }
            return antwoord;
        }
        public static bool CollectieCheckIsbn(int a)
        {
            //check als aanwezig in collectie
            string nummer, inhoud = "";
            bool gevonden = false;

            using StreamReader lezer = new StreamReader("Collectie.txt");
            inhoud = lezer.ReadToEnd();
            nummer = a.ToString();
            if (inhoud.Contains(nummer))
            {
                gevonden = true;
            }
            return gevonden;
        }   //AANGEPAST
        public static void TaxatieEdit()
        {
            //Editor bestaande taxaties
            int nummer;
            string boek = "", parameter = "", inhoud = "", boekEdit = "", parameterEdit;

            using StreamReader lezer = new StreamReader("Taxatie.txt");
            {
                inhoud = lezer.ReadToEnd();
            }
            lezer.Close();
            inhoud = inhoud.Remove(inhoud.LastIndexOf('*'));
            string[] arrTaxatie = inhoud.Split('*');
            for (int i = 0; i < (arrTaxatie.Length); i++)
            {
                Console.WriteLine($"{i + 1}. {arrTaxatie[i]}");
            }
            Console.WriteLine($"Geef het collectienummer in van het boek die u wilt aanpassen.");
            int.TryParse(Console.ReadLine(), out nummer);
            boek = arrTaxatie[nummer - 1];
            string[] arrBoek = boek.Split('|');
            for (int i = 0; i < (arrBoek.Length); i++)
            {
                Console.WriteLine($"{i + 1}.) {arrBoek[i]}");
            }
            Console.WriteLine($"Geef het parameternummer in van de gegevens die u wilt aanpassen.");
            int.TryParse(Console.ReadLine(), out nummer);
            parameter = arrBoek[nummer - 1];
            Console.WriteLine(parameter);
            Console.WriteLine("Geef de nieuwe waarde in waarmee je de oude wil overschrijven");
            parameterEdit = Console.ReadLine();
            if (!(parameterEdit == "") || !(parameterEdit.Contains('|')) || !(parameterEdit.Contains('*')))
            {
                arrBoek[nummer - 1] = parameterEdit;
                for (int i = 0; i < (arrBoek.Length - 1); i++)
                {
                    boekEdit += (arrBoek[i] + "|");
                }
                boekEdit += arrBoek[(arrBoek.Length) - 1] + "*";
                inhoud = inhoud.Replace($"{boek}", $"{boekEdit}");
                using (StreamWriter schrijver = new StreamWriter("Taxatie.txt"))
                {
                    schrijver.WriteLine(inhoud);
                }
            }
        }
        public static bool TaxatieCheckISBN(int a)
        {
            //Check als aanwezig in taxaties
            string nummer, inhoud = "";
            bool gevonden = false;

            using StreamReader lezer = new StreamReader("Taxatie.txt");
            inhoud = lezer.ReadToEnd();
            nummer = a.ToString();
            if (inhoud.Contains(nummer))
            {
                gevonden = true;
            }
            return gevonden;
        }
        public static bool TaxatieZoekISBN(int a)
        {
            int nummer;
            bool gevonden = false;
            using StreamReader lezer = new StreamReader("Taxatie.txt");
            string inhoud = "";
            inhoud = lezer.ReadToEnd();
            nummer = a;
            if (!(inhoud.Contains($"{nummer}")))
            {
                Console.WriteLine("Nummer niet teruggevonden, probeer opnieuw!");
            }
            else
            {
                Console.WriteLine("Nummer teruggevonden.");
                gevonden = true;
            }
            return gevonden;
        }
        public static string TaxatieDisplayResult(int a)
        {
            // Display Lijn met gegevens
            int i, j, l;

            using StreamReader lezer = new StreamReader("Taxatie.txt");
            string inhoud;
            inhoud = lezer.ReadToEnd();
            i = inhoud.IndexOf($"{a}");
            if (i != 0)
            {
                inhoud = inhoud.Remove(0, i);
                j = inhoud.IndexOf('*');
                l = inhoud.Length;
                inhoud = inhoud.Remove(j, (l - j));

            }
            else if (i == 0)
            {
                j = inhoud.IndexOf('*');
                l = inhoud.Length;
                inhoud = inhoud.Remove(j, (l - j));
            }
            return inhoud;
        }
        public static double TaxatieBerekenen(string staat)
        {
            //Prijsberekening op basis van staat
            Random taxRandom = new Random();
            double taxatiePrijs = 0;
            taxatiePrijs = taxRandom.Next(500, 10001);
            switch (staat)
            {
                case "Nieuw":
                    break;
                case "Gelezen":
                    taxatiePrijs *= 0.75;
                    break;
                case "Versleten":
                    taxatiePrijs *= 0.5;
                    break;
                case "Beschadigd":
                    taxatiePrijs *= 0.25;
                    break;
                default:
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine("De staat die ingevuld is voor dit boek klopt niet, gelieve dit na te kijken in de file Taxatie.txt en het daarna opnieuw te proberen.");
                    Console.ResetColor();
                    break;
            }
            return taxatiePrijs;
        }
        public static string CollectieDisplayResult(int a)  ////AANGEPAST
        {
            //display lijn gegevens uit collectie
            int i, j, l;

            using StreamReader lezer = new StreamReader("Collectie.txt");
            string inhoud;
            inhoud = lezer.ReadToEnd();
            i = inhoud.IndexOf($"{a}");
            if ((i != 0) && (i != -1))
            {
                inhoud = inhoud.Remove(0, (i));
                j = inhoud.IndexOf('*');
                l = inhoud.Length;
                inhoud = inhoud.Remove(j, (l - j));
            }
            else if (i != -1)
            {
                j = inhoud.IndexOf('*');
                l = inhoud.Length;
                inhoud = inhoud.Remove(j, (l - j));
            }
            else
            {
                inhoud = "";
            }

            return inhoud;
        }


        public static bool CollectieEdit()
        {
            //editor bestaande collectiegegevens
            int nummer;
            string boek = "", parameter = "", inhoud = "", boekEdit = "", parameterEdit, etiket;
            bool antwoord = false, inputCorrect = false;
            try
            {
                while (inputCorrect == false)
                {
                    using StreamReader lezer = new StreamReader("Collectie.txt");
                    {
                        inhoud = lezer.ReadToEnd();
                    }
                    lezer.Close();
                    inhoud.Remove(inhoud.LastIndexOf("*"), 1);
                    string[] arrCollectie = inhoud.Split('*');
                    for (int i = 0; i < (arrCollectie.Length - 1); i++)
                    {
                        Console.WriteLine($"{i + 1}. {arrCollectie[i]}");
                    }
                    Console.WriteLine($"Geef het collectienummer in van het boek die u wilt aanpassen.");
                    int.TryParse(Console.ReadLine(), out nummer);
                    boek = arrCollectie[nummer - 1];
                    string[] arrBoek = boek.Split('|');
                    for (int i = 0; i < (arrBoek.Length); i++)
                    {
                        Console.WriteLine($"{i + 1}.) {arrBoek[i]}");
                    }
                    Console.WriteLine($"Geef het parameternummer in van de gegevens die u wilt aanpassen.");
                    int.TryParse(Console.ReadLine(), out nummer);
                    parameter = arrBoek[nummer - 1];
                    Console.WriteLine(parameter);
                    Console.WriteLine("Geef de nieuwe waarde in waarmee je de oude wil overschrijven");
                    parameterEdit = Console.ReadLine();
                    if (!(parameterEdit == "") || !(parameterEdit.Contains('|')) || !(parameterEdit.Contains('*')))
                    {

                        boekEdit = boek.Replace($"{parameter}", $"{parameterEdit}");
                        inhoud = inhoud.Replace($"{boek}", $"{boekEdit}");
                        Console.Clear();
                        etiket = DrukEtiket(boekEdit);
                        Console.WriteLine(etiket);
                        Console.ReadLine();
                        using (StreamWriter schrijver = new StreamWriter("Collectie.txt"))
                        {
                            schrijver.WriteLine(inhoud);
                        }
                        inputCorrect = true;
                    }
                }
                antwoord = true;
                return antwoord;
            }
            catch
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine("Foute input, u gaat terug naar het menu!");
                Console.ResetColor();
                Console.ReadLine();

                antwoord = true;
                return antwoord;
            }
        }   //AANGEPAST
        public static bool CollectieRemove()
        {
            //Remove Entrys uit collectie
            int nummer, b;
            string boek = "", inhoud = "";
            bool antwoord, doorgaan = true;
            try
            {
                while (doorgaan == true)
                {
                    using StreamReader lezer = new StreamReader("Collectie.txt");
                    inhoud = lezer.ReadToEnd();
                    lezer.Close();
                    inhoud = inhoud.Remove(inhoud.LastIndexOf('*'));
                    string[] arrCollectie = inhoud.Split('*');
                    Array.Sort(arrCollectie);
                    for (int i = 0; i < arrCollectie.Length; i++)
                    {
                        Console.WriteLine($"{ i + 1}.) { arrCollectie[i]}");
                    }
                    Console.WriteLine($"Geef het collectienummer in van de entry die u wilt verwijderen. Druk return voor Collectiemenu");
                    int.TryParse(Console.ReadLine(), out nummer);
                    Console.WriteLine($" Boek : {arrCollectie[nummer - 1]} wordt verwijderd!");
                    Console.ReadLine();
                    if (nummer <= arrCollectie.Length)
                    {
                        boek = arrCollectie[nummer - 1];

                        int a = inhoud.IndexOf(boek);
                        b = boek.Length;
                        inhoud = inhoud.Remove(a, b);
                        using StreamWriter schrijver = new StreamWriter("Collectie.txt");
                        schrijver.WriteLine(inhoud);
                        schrijver.Close();
                    }
                    doorgaan = false;
                }
                doorgaan = true;
                antwoord = false;
                return antwoord;
            }
            catch
            {
                antwoord = true;
                return antwoord;

            }
        }   //AANGEPAST
        public static double PrijsBerekenen(double a)
        {
            //aankoop prijs + aankoop/2; (+50% winst) +21% btw!!
            double verkoopPrijs = 0;
            verkoopPrijs = ((a) * 1.5) * 1.21;
            return verkoopPrijs;
        }
        public static string TaxatieNieuw(int a)
        {
            //BASIS AANMAKEN GEGEVENS!
            string staatBoek = "", taxatieEntry = "";

            taxatieEntry += (a.ToString() + "|");
            Console.WriteLine("In welk jaar werd het boek uitgegeven?");
            taxatieEntry += (Console.ReadLine() + "|");
            Console.WriteLine("In welke staat bevindt het boek zich? Antwoord met de juiste letter: (N)ieuw, (G)elezen, (V)ersleten of (B)eschadigd.");
            staatBoek = Console.ReadLine();
            switch (staatBoek)
            {
                case "N":
                    staatBoek = "Nieuw";
                    taxatieEntry += (staatBoek);
                    break;
                case "G":
                    staatBoek = "Gelezen";
                    taxatieEntry += (staatBoek);
                    break;
                case "V":
                    staatBoek = "Versleten";
                    taxatieEntry += (staatBoek);
                    break;
                case "B":
                    staatBoek = "Beschadigd";
                    taxatieEntry += (staatBoek);
                    break;
                default:
                    Console.WriteLine("Gelieve enkel met 'N', 'G', 'V' of 'B' te antwoorden!");
                    break;
            }

            taxatieEntry += ("|" + TaxatieBerekenen(staatBoek).ToString() + "|");

            Console.WriteLine("Wie is de auteur van het boek?");
            taxatieEntry += (Console.ReadLine() + "|");
            Console.WriteLine("Wat is de titel van het boek?");
            taxatieEntry += Console.ReadLine();

            using (StreamWriter writer = new StreamWriter("Taxatie.txt", append: true))
            {
                writer.Write(taxatieEntry + "*");
            }

            return taxatieEntry;
        }
        public static string EtiketVerkoop(string a, ref string b)
        {
            string etiket, nummer, gebruiker, logo = @"  )
  ) \
 / ) (
 \(_)/ ";
            //Etiket Verkoop afdrukken
            gebruiker = b;
            Random gen = new Random();
            nummer = (gen.Next(1000000, 9999999)).ToString();
            Console.Clear();
            string[] arrBoek = a.Split('|');
            DateTime vandaag = DateTime.Now;
            etiket = logo;
            etiket += vandaag.ToString("dddd, dd MMMM yyyy HH:mm:ss");
            etiket += ($"VERKOOPSBEWIJS N° {nummer}\n" + "".PadLeft(20, '_') +
                $"{vandaag.ToString("dddd, dd MMMM yyyy HH:mm:ss")}" + "".PadRight(20, '_') + Environment.NewLine + "Titel :"
                + "".PadLeft(10, '-') + $"{arrBoek[5]}" + "".PadLeft(10, '-')
                + Environment.NewLine + "Auteur :" + "".PadLeft(10, '-') + $"{arrBoek[4]}" + "".PadLeft(10, '-') + Environment.NewLine +
                "Jaartal :" + "".PadLeft(10, '-') + $"{arrBoek[1]}" + "".PadLeft(10, '-') + Environment.NewLine +
                "Staat :" + "".PadLeft(10, '-') + $"{arrBoek[2]}" + "".PadLeft(10, '-')
                + Environment.NewLine + "Prijs :" + "".PadLeft(40, '*') + $"{arrBoek[3]} euro, incl 21% BTW" + Environment.NewLine + $"U werd geholpen door {gebruiker}" + Environment.NewLine +
                "Bedankt voor uw aankoop! En Happy Burning!" + Environment.NewLine + "Dit normaal nr een printer sturen hahaha");
            return etiket;
        }
        public static string EtiketAankoop(string a, ref string b)
        {
            //Idem Verkoop
            string etiket, nummer, gebruiker, logo = @"  )
  ) \
 / ) (
 \(_)/ ";
            gebruiker = b;
            Random gen = new Random();
            nummer = (gen.Next(1000000, 9999999)).ToString();
            Console.Clear();
            string[] arrBoek = a.Split('|');
            DateTime vandaag = DateTime.Now;
            etiket = logo;
            etiket += vandaag.ToString("dddd, dd MMMM yyyy HH:mm:ss");
            etiket += ($"AANKOOPSBEWIJS/TAXATIEBEWIJS N° {nummer}\n" + "".PadLeft(20, '_') +
                $"{vandaag.ToString("dddd, dd MMMM yyyy HH:mm:ss")}" + "".PadRight(20, '_') + Environment.NewLine + "Titel :"
                + "".PadLeft(10, '-') + $"{arrBoek[5]}" + "".PadLeft(10, '-')
                + Environment.NewLine + "Auteur :" + "".PadLeft(10, '-') + $"{arrBoek[4]}" + "".PadLeft(10, '-') + Environment.NewLine +
                "Jaartal :" + "".PadLeft(10, '-') + $"{arrBoek[1]}" + "".PadLeft(10, '-') + Environment.NewLine +
                "Staat :" + "".PadLeft(10, '-') + $"{arrBoek[2]}" + "".PadLeft(10, '-')
                + Environment.NewLine + "Prijs :" + "".PadLeft(40, '*') + $"{arrBoek[3]} euro, incl 21% BTW" + Environment.NewLine + $"U werd geholpen door {gebruiker}" + Environment.NewLine +
                "Bedankt voor uw vertrouwen in The Book Burners en Happy Burning!" + Environment.NewLine + "Dit normaal nr een printer sturen hahaha");
            Console.ReadLine();
            return etiket;
        }
        public static bool BackUp()
        {
            // Aanmaken van back up bestanden
            string inhoud, titel;
            bool antwoord;
            DateTime vandaag = DateTime.Now;
            titel = "BackupCollectie" + vandaag.ToString("ddMMMMyyyy") + ".txt";
            using StreamReader lezer = new StreamReader("Collectie.txt");
            inhoud = lezer.ReadToEnd();
            lezer.Close();
            using StreamWriter schrijver = new StreamWriter(titel);
            schrijver.WriteLine(inhoud);
            schrijver.Close();
            using StreamReader lezer1 = new StreamReader("Taxatie.txt");
            inhoud = lezer1.ReadToEnd();
            lezer1.Close();
            titel = "BackupTaxatie" + vandaag.ToString("ddMMMMyyyy") + ".txt";
            using StreamWriter schrijver1 = new StreamWriter(titel);
            schrijver1.WriteLine(inhoud);
            schrijver1.Close();
            using StreamReader lezer2 = new StreamReader("KassaInhoud.txt");
            inhoud = lezer2.ReadToEnd();
            lezer2.Close();
            titel = "BackupKassaInh" + vandaag.ToString("ddMMMMyyyy") + ".txt";
            using StreamWriter schrijver2 = new StreamWriter(titel);
            schrijver2.WriteLine(inhoud);
            schrijver2.Close();
            using StreamReader lezer3 = new StreamReader("KassaLog.txt");
            inhoud = lezer3.ReadToEnd();
            lezer3.Close();
            titel = "BackupKassaLog" + vandaag.ToString("ddMMMMyyyy") + ".txt";
            using StreamWriter schrijver3 = new StreamWriter(titel);
            schrijver3.WriteLine(inhoud);
            schrijver3.Close();

            Console.WriteLine("Back Ups aangemaakt, u gaat terug naar het hoofdmenu");
            Console.ReadLine();
            Console.Clear();
            antwoord = false;
            return antwoord;
        }    //AANGEPAST
        public static void Account(ref string a)  //AANGEPAST
        {
            //Controle Account voor Main menu, geeft gebruiker mee voor verder gebruik
            bool verdergaan = false;
            string gebruiker, paswoord, paswoord2, account;
            int j, l;
            if (!(System.IO.File.Exists("account.txt")))
            {
                while (verdergaan == false)
                {
                    using StreamWriter schrijver = new StreamWriter("account.txt");
                    Console.WriteLine($"Welkom bij het eerste gebruik van u software.\n\nGelieve een gebruikersnaam en paswoord te kiezen.\n\nGeef nieuwe gebruikersnaam in.");
                    gebruiker = Console.ReadLine();
                    Console.WriteLine("Geef een nieuw paswoord in.");
                    paswoord = Console.ReadLine();
                    Console.WriteLine("Bevestig je paswoord.");
                    paswoord2 = Console.ReadLine();
                    if (paswoord != paswoord2)
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine("Dit is niet hetzelfde, wij proberen opnieuw");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine($"Gebruiker = {gebruiker}\nPaswoord = {paswoord2}");
                        verdergaan = true;
                        a = gebruiker;
                        Console.Clear();
                    }
                    schrijver.Write(gebruiker + "|" + paswoord2 + "*");
                    schrijver.Close();

                }

            }
            else
            {
                while (verdergaan == false)
                {
                    using StreamReader lezer = new StreamReader("account.txt");
                    account = lezer.ReadToEnd();
                    lezer.Close();
                    Console.WriteLine("Geef Gebruikernaam in");
                    gebruiker = Console.ReadLine();
                    if (account.Contains(gebruiker))
                    {
                        int i = account.IndexOf($"{gebruiker}");
                        if (i != 0)
                        {
                            account = account.Remove(0, i);
                            j = account.IndexOf('*');
                            l = account.Length;
                            account = account.Remove(j, (l - j));
                        }
                        else
                        {
                            j = account.IndexOf('*');
                            l = account.Length;
                            account = account.Remove(j, (l - j));
                        }
                        string[] arrGebruiker = account.Split('|');
                        if (gebruiker != arrGebruiker[0])
                        {
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.WriteLine("Gebruiker Onbekend!");
                            Console.ReadLine();
                            Console.ResetColor();
                        }
                        else
                        {
                            Console.WriteLine("Geef Paswoord in");
                            paswoord = Console.ReadLine();
                            if (!(paswoord == arrGebruiker[1]))
                            {
                                Console.BackgroundColor = ConsoleColor.Red;
                                Console.ForegroundColor = ConsoleColor.Black;
                                Console.WriteLine("Fout Paswoord!");
                                Console.ResetColor();
                            }
                            else
                            {
                                verdergaan = true;
                                a = gebruiker;
                                Console.Clear();
                            }
                        }
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine("Foute input!");
                        Console.ResetColor();
                    }

                }
                verdergaan = false;
            }


        }
        public static bool AccountToevoegen()
        {
            //toevoegen van gebruikers/passwoorden
            bool antwoord, status = false;
            string account, input, gebruiker, paswoord, paswoord2;
            while (status == false)
            {
                using StreamReader lezer = new StreamReader("account.txt");
                account = lezer.ReadToEnd();
                lezer.Close();
                Console.WriteLine("Geef een niewe gebruikersnaam in.");
                gebruiker = Console.ReadLine();
                Console.WriteLine("Geef een paswoord in.");
                paswoord = Console.ReadLine();
                Console.WriteLine("Bevestig het paswoord.");
                paswoord2 = Console.ReadLine();
                if ((!(gebruiker == "") || !(gebruiker.Contains('|')) || !(gebruiker.Contains('*'))) && (!(paswoord == "") || !(paswoord.Contains('|')) || !(paswoord.Contains('*'))))
                {
                    if ((paswoord != paswoord2) || account.Contains(gebruiker))
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine("Dit is niet ok, gebruiker bestaat al/paswoord matched niet, wij proberen opnieuw");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine($"Gebruiker = {gebruiker}\nPaswoord = {paswoord2}");
                        status = true;
                        Console.ReadLine();
                    }
                    input = (gebruiker + "|" + paswoord2 + "*");
                    account += input;
                    using StreamWriter schrijver = new StreamWriter("account.txt");
                    schrijver.Write(account);
                    schrijver.Close();


                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine("Foute input");
                    Console.ResetColor();
                    Console.ReadLine();
                }

            }
            antwoord = true;
            return antwoord;

        }   //AAN te PASSEN, foute input opvangen " "|*
        public static bool KassaMain(ref double kassaInhoud)
        {
            bool antwoord = false, menuKassa = true;
            int kassaMenuKeuze = 0, aantalLijnenKassaLog = 0;
            double kassaEdit = 0;
            string euroTeken = @"
           _.-------._
        _-'_.------._ `-_
      _- _-          `-_/
     -  -
 ___/  /______________
/___  .______________/
 ___| |_____________
/___  .____________/
    \  \
     -_ -_             /|
       -_ -._        _- |
         -._ `------'_./
            `-------'";
            while (menuKassa == true)
            {
                Console.WriteLine(euroTeken);
                Console.WriteLine("\nKassa: " + kassaInhoud + " EUR");
                Console.WriteLine("\nKASSAMENU\n---------");
                Console.WriteLine($"1.)Bedrag in de kassa corrigeren\n\n2.)Geld aan de kassa toevoegen\n\n" +
                            $"3.)Geld uit de kassa nemen\n\n4.)Overzicht kassaverrichtingen\n\n5.)Terug naar Hoofdmenu\n\n");
                Console.WriteLine($"Geef uw keuze in 1/2/3/4:");
                while (!(int.TryParse(Console.ReadLine(), out kassaMenuKeuze)))
                {
                    Console.WriteLine("Gelieve een getal tussen 1 en 4 in te geven om uw keuze te maken.");
                }
                switch (kassaMenuKeuze)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Geef het nieuwe correcte bedrag in dat momenteel in de kassa zit:");
                        while (!(double.TryParse(Console.ReadLine(), out kassaInhoud)))
                        {
                            Console.WriteLine("Gelieve een getal in te geven!");
                        }
                        kassaInhoud = Math.Round(kassaInhoud, 2);
                        Console.WriteLine("Het bedrag in de kassa is aangepast naar: " + kassaInhoud + " EUR");
                        Console.ReadKey();
                        break;
                    case 2:
                        Console.WriteLine("Hoeveel wil je toevoegen aan de kassa?");
                        while (!(double.TryParse(Console.ReadLine(), out kassaEdit)))
                        {
                            Console.WriteLine("Gelieve een getal in te geven!");
                        }
                        kassaInhoud = Math.Round(KassaPlus(kassaInhoud, kassaEdit), 2);
                        Console.ReadKey();
                        break;
                    case 3:
                        Console.WriteLine("Hoeveel wil je uit de kassa nemen?");
                        while (!(double.TryParse(Console.ReadLine(), out kassaEdit)))
                        {
                            Console.WriteLine("Gelieve een getal in te geven!");
                        }
                        kassaInhoud = Math.Round(KassaMin(kassaInhoud, kassaEdit), 2);
                        Console.ReadKey();
                        break;
                    case 4:
                        string[] arrKassaLogs;
                        using (StreamReader reader = new StreamReader("KassaLog.txt"))
                        {
                            while (reader.ReadLine() != null)
                            {
                                aantalLijnenKassaLog++;
                            }
                        }
                        using (StreamReader reader = new StreamReader("KassaLog.txt"))
                        {
                            arrKassaLogs = new string[aantalLijnenKassaLog];   //Hier pas geïnitialiseerd omdat we nu aantal lijnen kennen en dus lengte van de array
                            int i = 0;
                            do
                            {
                                arrKassaLogs[i] = reader.ReadLine();
                                i++;
                            } while (i < aantalLijnenKassaLog);
                        }
                        arrKassaLogs = arrKassaLogs.Reverse().ToArray();
                        foreach (string a in arrKassaLogs)
                        {
                            Console.WriteLine(a);
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 5:
                        menuKassa = false;
                        break;
                }
            }
            return antwoord;
        }
        public static double KassaPlus(double totaal, double edit)
        {
            totaal += edit;
            using (StreamWriter writer = new StreamWriter("KassaLog.txt", append: true))
            {
                writer.WriteLine(DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm") + " : +" + edit + " EUR");
            }
            Console.WriteLine("Er is " + Math.Round(edit, 2) + " EUR aan de kassa toegevoegd.\nEr zit nu " + Math.Round(totaal, 2) + " EUR in de kassa.");
            return totaal;
        }
        public static double KassaMin(double totaal, double edit)
        {
            if (totaal - edit >= 0)
            {
                totaal -= edit;
                using (StreamWriter writer = new StreamWriter("KassaLog.txt", append: true))
                {
                    writer.WriteLine(DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm") + " : -" + edit + " EUR");
                }
                Console.WriteLine("Er is " + Math.Round(edit, 2) + " EUR uit de kassa genomen.\nEr zit nu nog " + Math.Round(totaal, 2) + " EUR in de kassa.");
            }
            else
            {
                Console.WriteLine("Er zit niet genoeg geld in de kassa! Deze actie zal niet uitgevoerd worden.");
            }
            return totaal;
        }
        public static string DrukEtiket(string a)
        {
            //Afdrukken Labels die op boeken geplakt moeten worden
            string etiket;
            Console.Clear();
            string[] arrBoek = a.Split('|');
            etiket = ("-" + arrBoek[0] + "-" + Environment.NewLine + "-" + arrBoek[1] + "-" + Environment.NewLine + "-" + arrBoek[2] + "-" + Environment.NewLine + "-" + arrBoek[3] + "-" + Environment.NewLine +
                "-" + arrBoek[4] + "-" + Environment.NewLine + "-" + arrBoek[5] + "-" + Environment.NewLine + "plak mij op een boek");
            return etiket;
        }  //AANGEPAST

        public static bool AccountMain()
        {
            //Main menu om Accounts te beheren, keuze maken
            int keuze;
            bool doorgaan = true, edit = false, remove = false, antwoord, aanmaken = false;
            string inhoud = "";

            while (doorgaan == true)
            {
                Console.WriteLine($"\nAccountMenu\n\n1.) Accounts Weergeven\n\n2.) Gebruiker/Passwoord aanpassen\n\n3.) Gebruikers Verbranden\n\n4.)Nieuewe gebruiker aanmaken\n\n5.)Terug Naar Hoofdmenu");
                int.TryParse(Console.ReadLine(), out keuze);

                switch (keuze)
                {
                    case 1:
                        using (StreamReader lezer = new StreamReader("account.txt"))
                            inhoud = lezer.ReadToEnd();
                        string[] arrAccount = inhoud.Split('*');
                        for (int i = 0; i < arrAccount.Length; i++)
                        {
                            Console.WriteLine(arrAccount[i]);
                        }
                        break;
                    case 2:
                        while (edit != true)
                        {
                            edit = AccountEdit();
                            Console.Clear();
                        }
                        break;
                    case 3:
                        while (remove != true)
                        {
                            remove = AccountRemove();
                            Console.Clear();
                        }
                        break;
                    case 4:
                        while (aanmaken != true)
                        {
                            aanmaken = AccountToevoegen();
                            Console.Clear();
                        }
                        break;
                    case 5:
                        doorgaan = false;
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Foute keuze!");
                        break;
                }
                remove = false;
                edit = false;
                aanmaken = false;
            }
            antwoord = false;
            return antwoord;
        }

        public static bool AccountEdit()
        {
            int nummer;
            string gegevens = "", parameter = "", inhoud = "", gegEdit = "", parameterEdit;
            bool antwoord = false, inputCorrect = false;
            try
            {
                while (inputCorrect == false)
                {
                    using StreamReader lezer = new StreamReader("account.txt");
                    {
                        inhoud = lezer.ReadToEnd();
                    }
                    lezer.Close();
                    inhoud.Remove(inhoud.LastIndexOf("*"), 1);
                    string[] arrAccount = inhoud.Split('*');
                    for (int i = 0; i < (arrAccount.Length - 1); i++)
                    {
                        Console.WriteLine($"{i + 1}. {arrAccount[i]}");
                    }
                    Console.WriteLine($"Geef het gebruikersnummernummer in van de gegevens die u wilt aanpassen.");
                    int.TryParse(Console.ReadLine(), out nummer);
                    gegevens = arrAccount[nummer - 1];
                    string[] arrGegevens = gegevens.Split('|');
                    for (int i = 0; i < (arrGegevens.Length); i++)
                    {
                        Console.WriteLine($"{i + 1}.) {arrGegevens[i]}");
                    }
                    Console.WriteLine($"Geef het parameternummer in van de gegevens die u wilt aanpassen.");
                    int.TryParse(Console.ReadLine(), out nummer);
                    parameter = arrGegevens[nummer - 1];
                    Console.WriteLine(parameter);
                    Console.WriteLine("Geef de nieuwe waarde in waarmee je de oude wil overschrijven");
                    parameterEdit = Console.ReadLine();
                    if (!(parameterEdit == "") || !(parameterEdit.Contains('|')) || !(parameterEdit.Contains('*')))
                    {

                        gegEdit = gegevens.Replace($"{parameter}", $"{parameterEdit}");
                        inhoud = inhoud.Replace($"{gegevens}", $"{gegEdit}");
                        Console.Clear();
                        Console.ReadLine();
                        using (StreamWriter schrijver = new StreamWriter("account.txt"))
                        {
                            schrijver.WriteLine(inhoud);
                        }
                        inputCorrect = true;
                    }
                }
                antwoord = true;
                return antwoord;
            }
            catch
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine("Foute input, u gaat terug naar het menu!");
                Console.ResetColor();
                Console.ReadLine();

                antwoord = true;
                return antwoord;
            }
        }

        public static bool AccountRemove()
        {
            int nummer, b;
            string gebruiker = "", inhoud = "";
            bool antwoord, doorgaan = true;
            try
            {
                while (doorgaan == true)
                {
                    using StreamReader lezer = new StreamReader("account.txt");
                    inhoud = lezer.ReadToEnd();
                    lezer.Close();
                    inhoud = inhoud.Remove(inhoud.LastIndexOf('*'));
                    string[] arrAccount = inhoud.Split('*');
                    Array.Sort(arrAccount);
                    for (int i = 0; i < arrAccount.Length; i++)
                    {
                        Console.WriteLine($"{ i + 1}.) { arrAccount[i]}");
                    }
                    Console.WriteLine($"Geef het collectienummer in van de entry die u wilt verwijderen. Druk return voor AccountMenu");
                    int.TryParse(Console.ReadLine(), out nummer);
                    Console.WriteLine($" Gebruiker : {arrAccount[nummer - 1]} wordt verwijderd!");
                    Console.ReadLine();
                    if (nummer <= arrAccount.Length)
                    {
                        gebruiker = arrAccount[nummer - 1];

                        int a = inhoud.IndexOf(gebruiker);
                        b = gebruiker.Length;
                        inhoud = inhoud.Remove(a, b);
                        using StreamWriter schrijver = new StreamWriter("account.txt");
                        schrijver.WriteLine(inhoud);
                        schrijver.Close();
                    }
                    doorgaan = false;
                }
                doorgaan = false;
                antwoord = false;
                return antwoord;
            }
            catch
            {
                antwoord = true;
                return antwoord;

            }
        }
    }
}




